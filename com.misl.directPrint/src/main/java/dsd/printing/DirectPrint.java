package dsd.printing;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.print.PrintService;

import oracle.forms.engine.Main;
import oracle.forms.engine.Message;
import oracle.forms.handler.IHandler;
import oracle.forms.properties.ID;
import oracle.forms.ui.CustomEvent;
import oracle.forms.ui.VBean;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class DirectPrint extends VBean {
	
	protected static final ID pLoadDirectPrint = ID
			.registerProperty("LOAD_DIRECT_PRINT");

	protected static final ID pSetTTFFromFS = ID
			.registerProperty("SET_TTF_FROM_FS");

	protected static final ID pSetTTFFromURL = ID
			.registerProperty("SET_TTF_FROM_URL");

	protected static final ID pDirectReportObject = ID
			.registerProperty("DIRECT_REPORT_OBJECT");

	protected static final ID pDirectPrintPDFURL = ID
			.registerProperty("DIRECT_PRINT_PDF_URL");
	
	protected static final ID pDirectPrintPDFURLToDefaultPrinter = ID
			.registerProperty("DIRECT_PRINT_PDF_URL_DEFAULT_PRINTER");

	protected static final ID pDefaultPrintService = ID
			.registerProperty("SET_DEFAULT_PRINT_SERVICE");

	protected static final ID pListClientPrinters = ID
			.registerProperty("LIST_CLIENT_PRINTERS");

	protected static final ID pEnableDebugging = ID
			.registerProperty("ENABLE_DEBUGGING");

	protected static final ID pWriteMessage = ID
			.registerProperty("WRITE_MESSAGE");

	protected static final ID pGetResult = ID.registerProperty("RESULT");
	protected static final ID GET_01 = ID.registerProperty("GET_01");

	protected String exception;
	protected  boolean isSuccessfull = true;

	/*
	 * Variables
	 */
	/**
	 * Forms Handler.
	 */
	private IHandler mHandler = null; // Forms Handler

	/**
	 * Forms Main Class.
	 */
	private Main formsMain = null; // Forms main class

	/**
	 * Debug toggle variable.
	 */
	private boolean debug_enabled = false;

	/**
	 * Default Instance Name.
	 */
	private String this_instance_name = this.getDefaultName();

	/**
	 * The protocol when a URL is built.
	 */
	private String protocol = null;

	/**
	 * The domain when a URL is built.
	 */
	private String domain = null;

	/**
	 * The port when a URL is built, the default value is 80.
	 */
	private int port = 80;

	// Using Fonts
	/**
	 * The external fonts to load from filesystem.
	 */
	private Properties extFSFonts = new Properties();

	/**
	 * The external fonts to load from URL.
	 */
	private Properties extURLFonts = new Properties();

	/**
	 * Named Printer - inited with the default printer
	 */
	private PrintService currentPrintService = PrinterJob.getPrinterJob()
			.getPrintService();

	/**
	 * Default Constructor.
	 */
	public DirectPrint() {
	}

	/**
	 * Default init.
	 */
	public final void init(IHandler handler) {
		System.out.println("Start");
		mHandler = handler;
		super.init(handler);
		// getting the Forms Main class
		formsMain = (Main) mHandler.getApplet();
		// Load defaults fonts that are used. -- User added.
		loadDefaultFSFonts();
		loadDefaultURLFonts();
	}

	public Object getProperty(ID property) {
		write_message(property.getName());
		Object result = new String("sdcsd");
		write_message(String.valueOf(isSuccessfull));
		
		if (property == pGetResult) {
			if (isSuccessfull) {
				result = new Boolean(true);

			} else {
				result = new String(exception == null ? "test" : exception);
			}

		}

		write_message("Test");
		return (Object) result;

	}

	// public Object getProperty(ID property) {
	// write_message(property.getName());
	//
	// write_message("dsfsdf");
	// if (property == GET_01) {
	// // return the corresponding value
	// return "the property needed";
	// } else // default behaviour
	// {
	// return super.getProperty(property);
	// }
	// }

	// *******************************
	// * SET FORMS PROPERTIES
	// *******************************
	/**
	 * Uses to set properties of the ID object.
	 * 
	 * @return The status of the set property
	 * @param _args
	 *            Value passed in from the forms bean call.
	 * @param _ID
	 *            The ID of the parameter that was called.
	 */
	public boolean setProperty(ID _ID, Object _args) {
		/*
		 * ===================== LOAD_DIRECT_PRINT=====================
		 */
		if (_ID == pLoadDirectPrint) {
			// check that argument is not null
			if ((_args != null)
					&& ((((String) _args).equalsIgnoreCase("TRUE")) || (((String) _args)
							.equalsIgnoreCase("FALSE")))) {
				try {
					write_message("LOAD_DIRECT_PRINT: " + (String) _args);

					if (((String) _args).equalsIgnoreCase("TRUE")) {
						// load classes
						loadDirectPrint();
					} else if (((String) _args).equalsIgnoreCase("FALSE")) {
						write_message("LOAD_DIRECT_PRINT called with value: "
								+ (String) _args);
					}
				} catch (Exception ex) {
					isSuccessfull = false;
					exception = ex.getMessage();
					write_message("Exception: " + ex.getMessage());
				}
			}

			return true;
		}

		/*
		 * ===================== DIRECT_REPORT_OBJECT=====================
		 */
		if (_ID == pDirectReportObject) {
			// check that argument is not null
			if ((_args != null)) {
				try {
					write_message("DIRECT_REPORT_OBJECT: " + (String) _args);
					this.parseURL();

					String pdfURL = "http" + "://" + domain + ":" + port
							+ (String) _args;
					// create the full URL
					write_message("Processing URL: " + pdfURL);
					// printPDFURL(pdfURL);
					boolean isPrinted = sendPDFURLToPrinter(pdfURL);
					if (!isPrinted)
						isSuccessfull = false;
					return false;
				} catch (Exception ex) {
					isSuccessfull = false;
					exception = ex.getMessage();
					write_message("Exception: " + ex.getMessage());
					return false;
				}
			}

			return true;
		}

		/*
		 * ===================== DIRECT_PRINT_PDF_URL=====================
		 */
		if (_ID == pDirectPrintPDFURL) {
			// check that argument is not null
			if ((_args != null)) {
				try {
					write_message("DIRECT_PRINT_PDF_URL: " + (String) _args);
					// printPDFURL((String) _args);
					boolean isPrinted = sendPDFURLToPrinter((String) _args);
					if (!isPrinted)
						return false;
				} catch (Exception ex) {
					write_message("Exception: " + ex.getMessage());
					return false;
				}
			}

			return true;
		}
		/*
		 * ===================== DIRECT_PRINT_PDF_URL_to_default_printer=====================
		 */
		if (_ID == pDirectPrintPDFURLToDefaultPrinter) {
			// check that argument is not null
			if ((_args != null)) {
				try {
					write_message("DIRECT_PRINT_PDF_URL: " + (String) _args);
					// printPDFURL((String) _args);
					boolean isPrinted = sendPDFURLToDefaultPrinter((String) _args);
					if (!isPrinted)
						return false;
				} catch (Exception ex) {
					write_message("Exception: " + ex.getMessage());
					return false;
				}
			}

			return true;
		}

		/*
		 * ========================= WRITE_MESSAGE ========================
		 */
		if (_ID == pWriteMessage) {
			if ((_args != null) && (((String) _args).length() != 0)) {
				write_message((String) _args);
			} else {
				// not valid argument was passed, print message to console
				write_message("Call to WRITE_MESSAGE cannot be null or zero length.");
			}

			return true;
		}

		/*
		 * ========================= ENABLE_DEBUGGING ========================
		 */
		if (_ID == pEnableDebugging) {
			if ((_args != null) && (((String) _args).length() != 0)) {
				if ("TRUE".equalsIgnoreCase((String) _args)) {
					debug_enabled = true;
					write_message("Setting debug_enable: true.");
				} else if ("FALSE".equalsIgnoreCase((String) _args)) {
					write_message("Setting debug_enable: false.");
					debug_enabled = false;
				}
			} else {
				// not valid argument was passed, print message to console
				write_message("Call to ENABLE_DEBUGGING does not have a valid argument.");
			}

			return true;
		}

		/*
		 * ========================= SET_DEFAULT_PRINT_SERVICE
		 * ========================
		 */
		if (_ID == pDefaultPrintService) {
			if ((_args != null) && (((String) _args).length() != 0)) {
				write_message("SET_DEFAULT_PRINT_SERVICE: " + (String) _args);
				this.setDefaultPrintServiceByName((String) _args);
			} else {
				// not valid argument was passed, print message to console
				write_message("Call to SET_DEFAULT_PRINT_SERVICE can not be a null argument.");
			}
			return true;
		}

		/*
		 * ========================= LIST_CLIENT_PRINTERS
		 * ========================
		 */
		if (_ID == pListClientPrinters) {
			if ((_args != null) && (((String) _args).length() != 0)) {
				if ("TRUE".equalsIgnoreCase((String) _args)) {

					write_message("LIST_CLIENT_PRINTERS: " + (String) _args);
					this.listClientPrinters(java.awt.print.PrinterJob
							.lookupPrintServices());

				} else if ("FALSE".equalsIgnoreCase((String) _args)) {
					write_message("Only a value of true will list client printers.");
				}
			} else {
				// not valid argument was passed, print message to console
				write_message("Call to LIST_CLIENT_PRINTERS does not have a valid argument.");
			}

			return true;
		}

		/*
		 * ========================= SET_TTF_FROM_FS ========================
		 */
		if (_ID == pSetTTFFromFS) {
			if ((_args != null) && (((String) _args).length() != 0)) {
				write_message("SET_TTF_FROM_FS: " + (String) _args);
				this.setFSFontProperty((String) _args);
			} else {
				// not valid argument was passed, print message to console
				write_message("Call to SET_TTF_FROM_FS can not be a null argument.");
			}
			return true;
		}

		/*
		 * ========================= SET_TTF_FROM_URL ========================
		 */
		if (_ID == pSetTTFFromURL) {
			if ((_args != null) && (((String) _args).length() != 0)) {
				write_message("SET_TTF_FROM_URL: " + (String) _args);
				this.setURLFontProperty((String) _args);
			} else {
				// not valid argument was passed, print message to console
				write_message("Call to SET_TTF_FROM_URL can not be a null argument.");
			}
			return true;
		}

		return super.setProperty(_ID, _args);
	}

	/**
	 * This is used in conjunction with the LOAD_DIRECT_PRINT
	 * 
	 * @return This is used in conjunction with the LOAD_DIRECT_PRINT
	 */
	private void loadDirectPrint() {
		write_message("Running loadDirectPrint");

		PDDocument document = null;

		try {
			document = new PDDocument();

			if (document != null) {
				document.close();
			}

			document = null;
		} catch (IOException ex) {

			isSuccessfull = false;
			exception = "Permission denied to print document.";
			write_message("Load IOException: " + ex.getMessage());
		} catch (Exception ex) {
			isSuccessfull = false;
			exception = "Permission denied to print document.";
			write_message("Load Exception: " + ex.getMessage());
		}

		write_message("loadDirectPrint Done");

	}

	/**
	 * This prints the URL to the currently set default printer.
	 * 
	 * @param pdfURLStr
	 *            Full URL to the PDF to print.
	 */
	public boolean sendPDFURLToPrinter(String pdfURLStr) {
		PDDocument document = null;

		try {
			URL pdfFile = new URL(pdfURLStr);
			write_message("Printing URL: " + pdfURLStr);
			document = PDDocument.load(pdfFile);
			List pages = document.getDocumentCatalog().getAllPages();

			// load fonts here iterate over pages
			if (pages.isEmpty()) {
				isSuccessfull = false;
				exception = "Pages list is empty";
				write_message("Pages list is empty");
				document.close();
				return false;
			} else {
				write_message("Pages list is NOT empty");
				Iterator it = pages.iterator();

				while (it.hasNext()) {
					PDPage currentPage = (PDPage) it.next();
					Map map = currentPage.findResources().getFonts();

					write_message("Map is: " + map.toString());
					Set mySet = map.keySet();

					Iterator setInt = mySet.iterator();

					while (setInt.hasNext()) {

						String val = (String) setInt.next();

						PDType1Font pdt = (PDType1Font) map.get(val);
						write_message("'" + val + " "
								+ pdt.getFontDescriptor().getFontName() + "'");

						PDFont currFont = this.loadFontByName(document, pdt
								.getFontDescriptor().getFontName(), val);

						if (currFont != null) {
							currentPage.findResources().getFonts()
									.put(val, currFont);
						}

					}
				}
			}

			AccessPermission currentPermissions = document
					.getCurrentAccessPermission();

			if (!currentPermissions.canPrint()) {
				isSuccessfull = false;
				exception = "Permission denied to print document.";
				write_message("Permission denied to print document.");
				document.close();
				return false;
			}

			PrinterJob printJob = PrinterJob.getPrinterJob();
			// printJob.setPrintService(currentPrintService);
			PrintService printService = choosePrinter();
			if (printService == null) {
				isSuccessfull = false;
				exception = "Printer not selected";
				document.close();
				return false;
			}

			printJob.setPrintService(printService);
			printJob.setPageable(document);

			// if (printJob.printDialog()) {
			write_message("Printing PDDocument");

			printJob.print();

			// Close document
			document.close();

		} catch (Exception ex) {
			write_message(ex.getMessage());
			isSuccessfull = false;
			exception = ex.getMessage();
			return false;
		}
		isSuccessfull = true;
		return true;
	}

	
	public boolean sendPDFURLToDefaultPrinter(String pdfURLStr) {
		PDDocument document = null;

		try {
			URL pdfFile = new URL(pdfURLStr);
			write_message("Printing URL: " + pdfURLStr);
			document = PDDocument.load(pdfFile);
			List pages = document.getDocumentCatalog().getAllPages();

			// load fonts here iterate over pages
			if (pages.isEmpty()) {
				isSuccessfull = false;
				exception = "Pages list is empty";
				write_message("Pages list is empty");
				document.close();
				return false;
			} else {
				write_message("Pages list is NOT empty");
				Iterator it = pages.iterator();

				while (it.hasNext()) {
					PDPage currentPage = (PDPage) it.next();
					Map map = currentPage.findResources().getFonts();

					write_message("Map is: " + map.toString());
					Set mySet = map.keySet();

					Iterator setInt = mySet.iterator();

					while (setInt.hasNext()) {

						String val = (String) setInt.next();

						PDType1Font pdt = (PDType1Font) map.get(val);
						write_message("'" + val + " "
								+ pdt.getFontDescriptor().getFontName() + "'");

						PDFont currFont = this.loadFontByName(document, pdt
								.getFontDescriptor().getFontName(), val);

						if (currFont != null) {
							currentPage.findResources().getFonts()
									.put(val, currFont);
						}

					}
				}
			}

			AccessPermission currentPermissions = document
					.getCurrentAccessPermission();

			if (!currentPermissions.canPrint()) {
				isSuccessfull = false;
				exception = "Permission denied to print document.";
				write_message("Permission denied to print document.");
				document.close();
				return false;
			}

			PrinterJob printJob = PrinterJob.getPrinterJob();
			 printJob.setPrintService(currentPrintService);
			//PrintService printService = choosePrinter();
			//if (printService == null) {
			//	isSuccessfull = false;
			//	exception = "Printer not selected";
			//	document.close();
			//	return false;
			//}

			//printJob.setPrintService(printService);
			printJob.setPageable(document);

			// if (printJob.printDialog()) {
			write_message("Printing PDDocument");

			printJob.print();

			// Close document
			document.close();

		} catch (Exception ex) {
			write_message(ex.getMessage());
			isSuccessfull = false;
			exception = ex.getMessage();
			return false;
		}
		isSuccessfull = true;
		return true;
	}

	
	private PrintService choosePrinter() {
		PrinterJob printJob = PrinterJob.getPrinterJob();
		if (printJob.printDialog()) {
			return printJob.getPrintService();
		} else {
			return null;
		}
	}

	/**
	 * This is used to set the protocol and host and port that the forms applet
	 * was launched.
	 * 
	 * @throws java.lang.Exception
	 */
	private void parseURL() throws Exception {
		try {
			write_message("Applet URL: "
					+ mHandler.getApplet().getDocumentBase().toString());

			URL myURL = new URL(mHandler.getApplet().getDocumentBase()
					.toString());
			//this.protocol = myURL.getProtocol();
			this.protocol = "http";
			this.domain = myURL.getHost();

			// SSL work around
			if (this.protocol.equalsIgnoreCase("https")) {
				write_message("SSL port detected - current protocol: "
						+ this.protocol);
				// if port not set default to port 443
				if (myURL.getPort() == -1) {
					this.port = 443;
				} else {
					this.port = myURL.getPort();
				}
			} else if (this.protocol.equalsIgnoreCase("http")) {
				// if port not set default to port 80
				if (myURL.getPort() == -1) {
					this.port = 80;
				} else {
					this.port = myURL.getPort();
				}
			} else {
				throw new Exception("Unsupported Protocol: " + this.protocol);
			}
		} catch (MalformedURLException ex) {
			isSuccessfull = false;
			exception = "Permission denied to print document.";
			write_message("MFURL Exception: " + ex.getMessage());
			throw new Exception("Error in parseURL");
		}
	}

	/**
	 * A method to toggle the output of debug code.
	 * 
	 * @param message
	 *            Value to display
	 */
	private void write_message(String message) {
		if (debug_enabled) {
			System.out.println("DEBUG MESSAGE " + this_instance_name + ": "
					+ message);
		}
	}

	/**
	 * This is the name of the printer that print jobs will be sent to.
	 * 
	 * @param printerName
	 *            Name of client printer to send jobs to.
	 */
	private void setDefaultPrintServiceByName(String printerName) {
		try {
			currentPrintService = this.getPrintServiceFromName(
					java.awt.print.PrinterJob.lookupPrintServices(),
					printerName);
		} catch (Exception ex) {
			write_message("Exception in setDefaultPrintServiceByName: "
					+ ex.getMessage());
		}
	}

	/**
	 * This is the printService that will become the default printer
	 * 
	 * @throws java.lang.Exception
	 * @return This is the printService that will become the default printer
	 * @param printerName
	 *            The exact name fo the printer to print to.
	 * @param printService
	 *            This is an array of printers of the client.
	 */
	private javax.print.PrintService getPrintServiceFromName(
			PrintService[] printService, String printerName) throws Exception {
		PrintService retPS = null;

		for (int i = 0; i < printService.length; i++) {
			javax.print.PrintService ps = printService[i];

			if (ps.getName().equalsIgnoreCase(printerName)) {
				retPS = ps;
				write_message("PrintService Match on: " + printerName + ":"
						+ ps.getName());
			} else {
				write_message(ps.getName() + ":" + printerName);
			}
		}

		if (retPS == null)
			throw new Exception("Printer Name not found.");

		return retPS;
	}

	/**
	 * This is used to list the printers on the client workstation.
	 * 
	 * @param pServers
	 *            Array of printers on client workstaion.
	 */
	private void listClientPrinters(PrintService[] pServers) {
		for (int i = 0; i < pServers.length; i++) {
			javax.print.PrintService ps = pServers[i];
			write_message("Client Printer " + i + ": " + ps.getName());
		}
	}

	/**
	 * If there are standard fonts that you use, just add them here and they
	 * will be loaded on every print job with out having to call SET_TTF_FROM_FS
	 * or SET_TTF_FROM_URL
	 */
	private void loadDefaultFSFonts() {
		// Example: extFSFonts.setProperty(fontName,fontLocation);
	}

	/**
	 * If there are standard fonts that you use, just add them here and they
	 * will be loaded on every print job with out having to call SET_TTF_FROM_FS
	 * or SET_TTF_FROM_URL
	 */
	private void loadDefaultURLFonts() {
		// Example: extURLFonts.setProperty(fontName,fontLocation);
	}

	/**
	 * Specify fontsName and location to load, value delimited by $.
	 * 
	 * @param fontNameDelimLocation
	 *            Font name delimiter Font Location in a single string.
	 */
	private void setFSFontProperty(String fontNameDelimLocation) {
		StringTokenizer st = new StringTokenizer(fontNameDelimLocation, "$");
		String fontName = st.nextToken();
		String fontLocation = st.nextToken();

		// check that font name does not already exist
		if (extFSFonts.getProperty(fontName) == null) {
			extFSFonts.setProperty(fontName, fontLocation);
			write_message("extFSFonts added: " + fontName);
		} else {
			write_message("extFSFonts already set: " + fontName);
		}

	}

	/**
	 * Specify fontsName and location to load, value delimited by $.
	 * 
	 * @param fontNameDelimLocation
	 *            Font name delimiter Font Location in a single string.
	 */
	private void setURLFontProperty(String fontNameDelimLocation) {
		StringTokenizer st = new StringTokenizer(fontNameDelimLocation, "$");
		String fontName = st.nextToken();
		String fontLocation = st.nextToken();

		// check that font name does not already exist
		if (extURLFonts.getProperty(fontName) == null) {
			extURLFonts.setProperty(fontName, fontLocation);
			write_message("extURLFonts added: " + fontName);
		} else {
			write_message("extURLFonts already set: " + fontName);
		}

	}

	/**
	 * PDFont object of the fontName and position, if found in extFSFonts and
	 * extURLFonts.
	 * 
	 * @return PDFont object.
	 * @param fontPosition
	 *            This is the font position in the PDF document.
	 * @param fontName
	 *            This is the exact name of the font in the PDF document.
	 * @param document
	 *            This is the document to process and add fonts to.
	 */
	private PDFont loadFontByName(PDDocument document, String fontName,
			String fontPosition) {
		PDFont currFont = null;

		try {
			String fontPropValue = null;

			if (extFSFonts.getProperty(fontName) == null) {
				write_message("Font not found: " + fontName);
			} else {
				currFont = PDTrueTypeFont.loadTTF(document,
						extFSFonts.getProperty(fontName));
				return currFont;
			}

			if (extURLFonts.getProperty(fontName) == null) {
				write_message("Font not found: " + fontName);
			} else {
				currFont = PDTrueTypeFont.loadTTF(document,
						extURLFonts.getProperty(fontName));
				return currFont;
			}

		} catch (IOException ex) {
			write_message("loadFontByName: " + ex.getMessage());
		}

		return currFont;
	}

	public void dispatch_event(ID id) {
		CustomEvent ce = new CustomEvent(mHandler, id);
		dispatchCustomEvent(ce);
	}

}
